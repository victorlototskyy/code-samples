<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
  require_once ("../../engine/engine.load.common.php");
  $_class = "tests";
  $_pagename = "Tests";
  $_actions = array("view", "update", "del");

  if (isset($_POST["data"]) && !empty($_POST["data"])) {

    $data = array();
    $_data = $_POST["data"];

    parse_str($_data, $data);

    if (isset($data["_action"]) && in_array($data["_action"], $_actions)) {
      $_action = $data["_action"];

      $_a = "\\modules\\" . $_class;
      $_class = new $_a();

      $activeClass = new $_class();
      switch ($_action) {
        case "update":
        {
          if (!$data["_id"]) {
            if ($activeClass->add($data)) {
              $result = array(
                "title" => $_pagename,
                "text" => $lang->message["doneadd"] . "!",
                "type" => "success",
                "callback" => "closePopup",
                "args" => true
              );
            } else {
              $result = array(
                "title" => $_pagename,
                "text" => $lang->message["wegoterror"] . "!",
                "type" => "error",
                "callback" => "closePopup",
                "args" => true
              );
            }
              break;
          } else {
            if ($a = $activeClass->update($data)) {
              $result = array(
                "title" => $_pagename,
                "text" => $lang->message["doneupdate"] . "!",
                "type" => "success",
                "callback" => "updateTableData",
                "args" => $a);
            } else {
            }
            break;
          }
        }
        case "del":
        {
          if ($activeClass->delete($data["id"])) {
            $result = array(
              "title" => $_pagename,
              "text" => $lang->message["donedel"] . "!",
              "type" => "success",
              "callback" => "list_refresh",
            );
          } else {
            $result = array(
              "title" => $_pagename,
              "text" => $lang->message["wegoterror"] . "!",
              "type" => "error",
              "callback" => "list_refresh",
            );
          }
        }
      }
    }
  }

  echo (json_encode($result));
}
?>