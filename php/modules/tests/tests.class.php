<?php
namespace modules {

  use db\dbpdo as db;
  use backend\SETTINGS as settings;
  use backend\LISTING as listing;
  use secure\SECURE as secure;

  class tests extends \common\abstractmodules
  {
    protected $db;
    protected $_arr=array();
    protected $sort="id";

    public function __construct()
    {
      $this->db=DB::getInstance();
      $this->settings=SETTINGS::getInstance();
      $this->secure=SECURE::GetInstance();
    }

    public function add($arr)
    {

      if (!empty($arr)) {
        $data["name"]=(isset($arr["ftitle"])) ?$arr["ftitle"] : false;
        $data["active"]=(isset($arr["fstatus"])) ?$arr["fstatus"] : false;

        $this->db->reset();
        $this->db->assign("name", $data["name"]);
        $this->db->assign("active", $data["active"]);
        $_last_id=$this->db->insert($this->tblTests);

        if ($_last_id) {
          $this->addSourcesDetails($arr, $_last_id);
          return $_last_id;
        } else {
          return false;
        }
      }
    }

    public function update($arr)
    {
      if (!empty($arr)) {
        $data["name"]=(isset($arr["ftitle"])) ?$arr["ftitle"] : false;
        $data["active"]=(isset($arr["fstatus"])) ?$arr["fstatus"] : false;
        $data["_id"]=(isset($arr["_id"])) ?$arr["_id"] : false;

        if ($data["_id"]) {
          $this->db->reset();
          $this->db->assign("id", $data["_id"]);
          $this->db->assign("name", $data["name"]);
          $this->db->assign("active", $data["active"]);
          $this->db->update($this->tblTests, " where id = :id");

          $this->addSourcesDetails($arr, $data["_id"]);
          $this->updateSourcesDetails($arr, $data["_id"]);

          return $this->getRecordAfterUpdate($data["_id"]);
        } else {
          return false;
        }

      }
    }

    public function delete($id)
    {
      $this->db->reset();
      $this->db->assign("id", $id);

      $query="select
                  id from " . $this->tblTests . "
                  where id = :id";

      if ($this->db->q($query)->limit()->is_exists()) {

        $this->db->reset();
        $this->db->assign("active", 0);
        $this->db->assign("id", $id);
        $this->db->update($this->tblTests, " where id = :id limit 1");

        $this->db->reset();
        $this->db->assign("active", 0);
        $this->db->assign("test_id", $id);
        $this->db->update($this->tblTestHasSources, " where test_id = :test_id");
        return true;
      } else {
        return false;
      }
    }

    private function addSourcesDetails($arr, $id)
    {
      if (isset($arr["fdrugs"])) {
        foreach ($arr["fdrugs"] as $source_id=>$drugs) {
          foreach ($drugs as $key=>$drug) {
            $this->db->reset();
            $this->db->assign("test_id", $id);
            $this->db->assign("source_id", $source_id);
            $this->db->assign("drug_id", $drug);

            $this->db->assign("name", (isset($arr["ffname"][$source_id][$key])) ?$arr["ffname"][$source_id][$key] : false);
            $this->db->assign("value", (isset($arr["ffvalue"][$source_id][$key])) ?$arr["ffvalue"][$source_id][$key] : false);
            $this->db->assign("measure", (isset($arr["ffmeasure"][$source_id][$key])) ?$arr["ffmeasure"][$source_id][$key] : false);
            $this->db->insert($this->tblTestHasSources);
          }
        }
      }
    }

    private function updateSourcesDetails($arr, $id)
    {
      if (isset($arr["fedrugs"])) {
        foreach ($arr["fedrugs"] as $source_id=>$drugs) {
          foreach ($drugs as $key=>$drug) {
            $this->db->reset();
            $this->db->assign("id", $key);
            $this->db->assign("test_id", $id);
            $this->db->assign("source_id", $source_id);
            $this->db->assign("drug_id", $drug);

            $this->db->assign("name", (isset($arr["fename"][$source_id][$key])) ?$arr["fename"][$source_id][$key] : false);
            $this->db->assign("value", (isset($arr["fevalue"][$source_id][$key])) ?$arr["fevalue"][$source_id][$key] : false);
            $this->db->assign("measure", (isset($arr["femeasure"][$source_id][$key])) ?$arr["femeasure"][$source_id][$key] : false);
            $this->db->update($this->tblTestHasSources, " where id = :id");
          }
        }
      }
    }

    public function removeDrug($id) {
      $this->db->reset();
      $this->db->assign("id", $id);
      $this->db->assign("active", 0);
      $this->db->update($this->tblTestHasSources, " where id = :id");
    }

    public function view($id)
    {

    }

    public function getRecords($params)
    {
      $_status=1;

      $query="select SQL_CALC_FOUND_ROWS
                  t.name,
                  t.id,
                  count(ths.test_id) as 'drugs'
                from " . $this->tblTests . " t
                left join ".$this->tblTestHasSources." ths on ths.test_id = t.id
                where t.active = '" . $_status . "'";
      $this->db->q($query)->group("t.id")->order($this->sort, $this->order)->select();

      if ($this->db->result) {
        $result["data"]=$this->db->result;
        $result["sort"]=$this->sort;
        $result["order"]=$this->order;
        return $result;
      } else {
        return false;
      }
      return false;
    }

    public function getRecordsForSelect($params=array())
    {

      $_status=(isset($params["status"])) ?$params["status"] : 1;

      $this->db->reset();
      $this->db->assign("active", $_status);
      $query="select SQL_CALC_FOUND_ROWS
                  s.name,
                  s.id
                from " . $this->tblTests . " s
                where active = :active
                order by s.name";
      $this->db->q($query)->select();

      if ($this->db->result) {
        $result=$this->db->result;
      } else {
        $result=array();
      }
      return $result;
    }

    private function getRecordAfterUpdate($id)
    {
      $this->db->reset();
      $this->db->assign("id", $id);
      $query="select SQL_CALC_FOUND_ROWS
                  t.name,
                  count(ths.test_id) as 'drugs'
                from " . $this->tblTests . " t
                left join ".$this->tblTestHasSources." ths on ths.test_id = t.id
                where t.id = :id";
      $this->db->q($query)->group("t.id")->limit()->select();
      if ($this->db->result) {
        $result=array_values($this->db->result[0]);
      } else {
        $result=false;
      }

      return array(
        "id"=>$id,
        "code"=>$this->getClassName(),
        "data"=>$result
      );
    }

    public function getRecord($id)
    {
      $this->db->reset();
      $this->db->assign("id", $id);

      $query="select
          s.name,
          s.id,
          s.active as 'active'
        from " . $this->tblTests . " s
        where s.id = :id";
      $this->db->q($query)->limit()->select();
      if ($this->db->result) {
        $result["data"]=$this->db->result[0];
        $result["sources"]=$this->getTestHasSources($id);
      } else {
        $result["data"]=array(
          "name"=>false,
          "active"=>true
        );
      }

      return $result;
    }

    public function getTestHasSources($test_id) {
      $this->db->reset();
      $this->db->assign("test_id", $test_id);
      $query = "select
                  *
                from ".$this->tblTestHasSources."
                where test_id = :test_id and active = '1'";
      $this->db->q($query)->select();
      if ($this->db->result) {
        foreach ($this->db->result as $value) {
          $result[$value["source_id"]][] = $value;
        }
      } else {
        $result = false;
      }

      return $result;
    }

    public function getTestSources($test_id) {
      $result = false;

      $this->db->reset();
      $this->db->assign("test_id", $test_id);
      $query = "select
                  sources.name as 'source'
                from ".$this->tblTestHasSources." test_has_sources
                  left join ".$this->tblSources." sources on sources.id = test_has_sources.source_id
                where test_has_sources.test_id = :test_id and test_has_sources.active = '1'";
      $this->db->q($query)->group("test_has_sources.source_id")->select();
      if ($this->db->result) {
        $result = $this->db->result;
      }

      return $result;
    }

  }
}
?>