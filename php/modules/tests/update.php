<ul class="nav nav-tabs">
  <li class="active"><a href="#general_information" data-toggle="tab"><strong>General Information</strong></a></li>
  <?
  foreach ($this->tabNames as $tabName) {
    ?>
    <li><a href="#tab<?= $tabName["id"] ?>" data-toggle="tab"><strong><?= $tabName["name"] ?></strong></a></li>
  <?
  }
  ?>

</ul>
<div class="tab-content mb30">
  <div class="tab-pane form-horizontal active" id="general_information">
    <div class="form-group">
      <label class="col-sm-2 control-label" for="ftitle">Title <span class="asterisk">*</span></label>

      <div class="col-sm-4">
        <input class="form-control" required="" type="text" name="ftitle" id="ftitle"
               value="<?= $this->data["name"] ?>" placeholder="Type Name...">
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-4 col-sm-offset-2">
        <div class="ckbox ckbox-default">
          <input type="checkbox" value="1" name="fstatus" id="fstatus" <?= ($this->data["active"]) ?'checked' : '' ?>/>
          <label for="fstatus">Status</label>
        </div>
      </div>
    </div>

  </div>
  <?
  foreach ($this->tabNames as $tabName) {
    ?>
    <div class="tab-pane form-horizontal" id="tab<?= $tabName["id"] ?>">
      <div class="js-drugs">
        <?
        if (isset($this->sources[$tabName["id"]])) {
          foreach ($this->sources[$tabName["id"]] as $drug) {
            ?>
            <div>
              <div class="panel panel-default">
                <div class="panel-body">


                  <div class="form-group">
                    <label class="col-sm-2 control-label mt10">Drugs <span class="asterisk">*</span></label>

                    <div class="col-sm-4">
                      <select class="form-control js-drugs-select-e"
                              name="fedrugs[<?= $tabName["id"] ?>][<?= $drug["id"] ?>]">
                        <?
                        if (isset($this->drugs[$tabName["id"]])) {
                          foreach ($this->drugs[$tabName["id"]] as $option) {
                            ?>
                            <option value="<?= $option["id"] ?>"
                                    data-name="<?= $option["name"] ?>"
                                    data-value="<?= $option["value"] ?>"
                                    data-measure="<?= $option["measure"] ?>"
                                <?= selected($option["id"], $drug["drug_id"]) ?>
                                ><?= $option["name"] ?></option>
                          <?
                          }
                        }
                        ?>

                      </select>
                    </div>
                    <div class="col-sm-1 mt10">
                      <a href="javascript:;" class="js-remove-drug-e text-danger" rel="<?= $drug["id"] ?>">Remove</a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-offset-1 col-sm-1 control-label mt10">Name <span
                          class="asterisk">*</span></label>

                    <div class="col-sm-5">
                      <input class="form-control js-name" type="text"
                             value="<?= $drug["name"] ?>" name="fename[<?= $tabName["id"] ?>][<?= $drug["id"] ?>]"
                             placeholder="Type Name...">
                    </div>
                    <label class="col-sm-1 control-label mt10">Value <span class="asterisk">*</span></label>

                    <div class="col-sm-1">
                      <input class="form-control js-value" type="text"
                             value="<?= $drug["value"] ?>" name="fevalue[<?= $tabName["id"] ?>][<?= $drug["id"] ?>]"
                             placeholder="Value...">
                    </div>
                    <label class="col-sm-1 control-label mt10 col-nowrap">Measure <span
                          class="asterisk">*</span></label>

                    <div class="col-sm-1">
                      <input class="form-control js-measure" type="text"
                             value="<?= $drug["measure"] ?>" name="femeasure[<?= $tabName["id"] ?>][<?= $drug["id"] ?>]"
                             placeholder="Measure...">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?
          }
        }
        ?>
      </div>
      <a href="javascript:;" class="js-add-drug">Add New Drug</a>
    </div>
  <?
  }
  ?>
</div>

<div id="_drugs" style="display: none">

  <div class="panel panel-default">
    <div class="panel-body">


      <div class="form-group">
        <label class="col-sm-2 control-label mt10">Drugs <span class="asterisk">*</span></label>

        <div class="col-sm-4">
          <select class="form-control js-drugs-select"></select>
        </div>
        <div class="col-sm-1 mt10">
          <a href="javascript:;" class="js-remove-drug text-danger">Remove</a>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-offset-1 col-sm-1 control-label mt10">Name <span
              class="asterisk">*</span></label>

        <div class="col-sm-5">
          <input class="form-control js-name" type="text"
                 value="" placeholder="Type Name...">
        </div>
        <label class="col-sm-1 control-label mt10">Value <span class="asterisk">*</span></label>

        <div class="col-sm-1">
          <input class="form-control js-value" type="text"
                 value="" placeholder="Value...">
        </div>
        <label class="col-sm-1 control-label mt10 col-nowrap">Measure <span
              class="asterisk">*</span></label>

        <div class="col-sm-1">
          <input class="form-control js-measure" type="text"
                 value="" placeholder="Measure...">
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(".tab-pane").on("click", ".js-add-drug", function () {
    $this = $(this);

    el = $("#_drugs").clone().removeAttr("id").show();


    _source = $($this).closest(".tab-pane").attr("id");
    $.post("modules/tests/common.add.new.drug.php", {
      source: _source
    })
        .done(function (data) {
          var result = $.parseJSON(data);

          if (result.options) {
            $(el).find(".js-drugs-select").html(result.options);
          }
        });

    $($this).closest(".tab-pane").find(".js-drugs").append(el);
  })

  $(".tab-pane").on("click", ".js-remove-drug", function () {
    $(this).closest(".panel").remove();
  })

  $(".tab-pane").on("click", ".js-remove-drug-e", function () {
    if (confirm("Are you sure?")) {
      $.post("modules/tests/common.remove.drug.php", {
        id: $(this).attr("rel")
      })
      $(this).closest(".panel").remove();
    }
  })

  $(".js-drugs").on("change", ".js-drugs-select", function () {
    selected = $(this).find(":selected");
    parent = $(this).closest(".panel-body");
    source_id = ($(this).closest(".tab-pane").attr("id")).substr(3);
    _name = "[" + source_id + "][]";
    $(parent).find(".js-drugs-select").attr("name", ("fdrugs" + _name)).attr("required", true);
    $(parent).find(".js-name").val(selected.attr("data-name")).attr("name", ("ffname" + _name)).attr("required", true);
    $(parent).find(".js-value").val(selected.attr("data-value")).attr("name", ("ffvalue" + _name)).attr("required", true);
    $(parent).find(".js-measure").val(selected.attr("data-measure")).attr("name", ("ffmeasure" + _name)).attr("required", true);
  })

  $(".js-drugs").on("change", ".js-drugs-select-e", function () {
    selected = $(this).find(":selected");
    parent = $(this).closest(".panel-body");
    $(parent).find(".js-name").val(selected.attr("data-name"));
    $(parent).find(".js-value").val(selected.attr("data-value"));
    $(parent).find(".js-measure").val(selected.attr("data-measure"));
  })

</script>

<style>
  .modal-dialog {
    width: 1000px;
  }

  .col-nowrap {
    white-space: nowrap;
  }

</style>