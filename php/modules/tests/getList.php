<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
  require_once ("../../engine/engine.load.common.php");

  if (isset($_POST["data"]) && !empty($_POST["data"])) {
    $_class = $_POST["data"]["page"];
    $_a = "\\modules\\".$_class;
    $activeClass = new $_a();

    $result = $activeClass->getRecords(false);
    $page_code = $_POST["data"]["page"];

    if ($result != false) {
      $view = new \services\view_view("/__admin/modules/".$page_code."/");

      $view->set("result", $result);
      $view->set("page_code", $page_code);
      $view->set("lang", $lang);
      $view->set("sort", $result["sort"]);
      $view->set("order", $result["order"]);
      $view->set("perm", $secure->Permissions[$page_code]);

      echo $view->display("tview.php");
    } else {
      ?>
    <p class="norecords"><span class="smile">:(</span><br> <?=$lang->message["empty"]?></p>

    <?
    }
  }

}
?>