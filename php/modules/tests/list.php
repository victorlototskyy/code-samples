<?
$_class = "\\modules\\".$engine->getCurrentPage["page_code"];

$activeClass = new $_class();
$result = $activeClass->getRecords(false);
$page_code = $engine->getCurrentPage["page_code"];

if ($result != false) {
  $view = new \services\view_view("/__admin/modules/".$page_code."/");

  $view->set("result", $result);
  $view->set("page_code", $page_code);
  $view->set("lang", $lang);
  $view->set("perm", $secure->Permissions[$page_code]);

  echo $view->display("tview.php");
} else {
  ?>
<p class="norecords"><span class="smile">:(</span><br> <?=$lang->message["empty"]?></p>
<?
}
?>

