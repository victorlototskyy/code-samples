<form action="admin.php?page=<?= $this->page_code ?>" method="POST">
  <div class="table-responsive">
    <table class="table table-hover" id="table_<?= $this->page_code ?>">
      <thead>
      <tr>
        <th>Name</th>
        <th># of drugs</th>
        <th width="100">Action</th>
      </tr>
      </thead>
      <tbody>
      <? foreach ($this->result["data"] as $key=>$value) { ?>
        <tr class="odd gradeA">
          <td><?= $value["name"] ?></td>
          <td><?= $value["drugs"] ?></td>
          <td class="table-action">
            <a class="update" alt="Update record" href="javascript:;" rel="<?= $value["id"] ?>">
              <i class="fa fa-pencil"></i>
            </a>
            <a class="delete-row delete" href="javascript:;" rel="<?= $value["id"] ?>">
              <i class="fa fa-trash-o"></i>
            </a>
          </td>
        </tr>
      <? } ?>


      </tbody>
    </table>
  </div>
  <!-- table-responsive -->
</form>
<script>

  jQuery(document).ready(function () {

    jQuery('#table_<?= $this->page_code ?>').dataTable({
      "sPaginationType": "full_numbers",
      columnDefs: [
        { orderable: false, targets: [-1] }
      ]
    });

    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });

  });
</script>


