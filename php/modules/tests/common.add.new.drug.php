<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
  require_once("../../engine/engine.load.common.php");

  if (isset($_POST["source"]) && !empty($_POST["source"])) {
    $_source=substr($_POST["source"], 3);

    $params = array(
      "source"=>$_source,
      "status"=>1
    );

    $drugs=new \modules\drugs();
    $options = '<option value="">- please select -</option>';

    foreach ($drugs->getRecordsForOptions($params) as $drug) {
      $options.='<option value="' . $drug["id"] . '"
      data-name="'.$drug["name"].'"
      data-value="'.$drug["value"].'"
      data-measure="'.$drug["measure"].'"
      >' . $drug["name"] . '</option>';
    }

    $result=array(
      "options"=>$options,
      "type"=>"success"
    );

  } else {
    $result=false;
  }

  echo(json_encode($result));


}
?>