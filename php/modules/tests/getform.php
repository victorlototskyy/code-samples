<?
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
  require_once ("../../engine/engine.load.common.php");

  $_action = array("view", "add", "update");

  if ((isset($_POST["module"])) && (isset($_POST["type"])) && (in_array($_POST["type"], $_action))) {
    $_class = $_POST["module"];
    $_do = $_POST["type"];
    $_id = (isset($_POST["id"]) && is_numeric($_POST["id"]))?$_POST["id"]:false;

    $view = new \services\view_view("/__admin/modules/".$_class."/");

    $_a = "\\modules\\".$_class;
    $activeClass = new $_a();
//    var_dump($activeClass);

    switch ($_do) {
      case "update":
      {
        $_module = "update";
        $_taction = $lang->title["update_record"];
        $_buttons = array(
          '<button type="button" class="btn btn-default" data-dismiss="modal">'.$lang->title["cancel"].'</button>',
          '<button type="button" class="btn btn-primary submit">'.$lang->title["update_record"].'</button>'
        );

        $_data = $activeClass->getRecord($_id);

        $view->set("lang", $lang);
        $view->set("data", $_data["data"]);
        $view->set("sources", $_data["sources"]);

        $_sources = new \modules\sources();
        $view->set("tabNames", $_sources->getRecordsForSelect());

        $_drugs = new \modules\drugs();
        foreach ($_drugs->getRecordsForOptions() as $drug) {
          $_drugs_options[$drug["source"]][] = $drug;
        }

        $view->set("drugs", $_drugs_options);
//        $view->set("drugs", $_drugs->getRecordsForSelect());




        $_note = "<p>".$lang->message["noterequired"]."</p>";

        break;
      }
      case "add":
      {
        $_module = "update";
        $_taction = $lang->title["add_record"];
        $_buttons = array(
          '<button type="button" class="btn btn-default" data-dismiss="modal">'.$lang->title["cancel"].'</button>',
          '<button type="button" class="btn btn-primary submit">'.$lang->title["add_record"].'</button>'

        );

        $_data = $activeClass->getRecord(false);
        $view->set("data", $_data["data"]);
        $_sources = new \modules\sources();
        $view->set("tabNames", $_sources->getRecordsForSelect());

        $_drugs = new \modules\drugs();
        $view->set("drugs", $_drugs->getRecordsForSelect());

        $_note = "<p>".$lang->message["noterequired"]."</p>";
        break;
      }
      case "view":
      {
        $_module = "view";
        $_taction = $lang->title["view_record"];
        $_buttons = array(
          '<button type="button" class="btn btn-default" data-dismiss="modal">'.$lang->title["cancel"].'</button>',
        );

        $_data = $activeClass->view($_id);
        $view->set("data", $_data["data"]);
        $_note = "";
        break;
      }
    }

    $content = $view->display($_module.'.php');

    echo (json_encode(
      array("form" => $content,
            "buttons" => $_buttons,
            "action" => $_module,
            "taction" => $_taction,
            "note" => $_note

      )
    ));
    ?>

  <?
  }
}
?>