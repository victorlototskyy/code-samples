/**
 * @Static
 * @Class
 * @type {{getValueForRank: Function}}
 */
var Deck = { /** @lends Deck */
    /**
     * Return card value
     * @param rank {String} Card rank
     * @returns value {Integer} Integer value of given card
     */
    getValueForRank: function(rank){
        switch(rank){
            case "A":
                return 14; break;
            case "K":
                return 13; break;
            case "Q":
                return 12; break;
            case "J":
                return 11; break;
            case "T":
                return 10; break;
            default:
                return parseInt(rank, 10);
        }
    }
}