/**
 * Following Omaha-Low-High rules, determine stronger hand between two. Contain separate comparison functions for
 * High Hand and Low 8 hands.
 * @Class
 * @constructor
 */
var Comparator = function(){ /** @lends Comparator */

    /**
     * Determine which of two hands is stronger for High Hand. Receive hands after they passed through {Evaluator}
     * <br>Alghoritm:
     * <br>1. Sort hands by their strength
     * <br>2. If hands strength is different, one with bigger is winner
     * <br>3. Otherwise proceed to evaluation of hands with same strength
     * <br>3.1 Check if combination is Flush or StraightFlush, if it is
     * <br>3.1.1 Compare valuableRank properties, if those are same
     * <br>3.1.2 Loop through kickers to determine winner, if those are same it's a tie
     * <br>3.1.3 If either valuableRank or kicker of one of the hands is bigger than other, that hand wins
     * <br>3.2 Check if combination is Straight, if it is
     * <br>3.2.1 Compare rank of card which start Straight, if any of hands has bigger rank, it win, otherwise it's a tie
     * <br>3.3 Check if combination is FourOfKind, ThreeOfKind or HighCard, if it is
     * <br>3.3.1 Compare valuableRank properties, if those are same
     * <br>3.3.2 Loop through kickers to determine winner, if those are same it's a tie
     * <br>3.4 Check if combination is TwoPairs or Pair, if it is
     * <br>3.4.1 Compare pair properties which hold unique ranks for pairs, if those are same
     * <br>3.4.2 Loop through kickers to determine winner, if those are same it's a tie
     * <br>3.5 Check if combination is FullHouse, if it is
     * <br>3.5.1 Compare startEndRanks property values. First one will match to 3-cards combination, second to 2-cards
     * <br>3.5.2. If those are same, it's a tie
     * <br>4. Finally, compute results into human friendly resultLiteral string and return winning hand. If it's tie,
     * proper resultLiteral string created and first hand set as winner for data consistency.
     * @param hand1 {{}} HandA Object for comparing
     * @param hand2 {{}} HandB Object for comparing
     * @returns {{}} Winner Hand Object
     */
    this.compareHighHands = function(hand1, hand2){
        var hands = [hand1, hand2];
        var winner = {};
        var tie = false;

        var h = hands.sort(function(hand1, hand2){
            return hand2.strength - hand1.strength;
        });

        if(h[0].strength == h[1].strength){
            if(h[0].isFlush || h[0].isStraightFlush){
                if(h[0].valuableRank == h[1].valuableRank){
                    for(var i = 0; i < 4; i++){
                        if(h[0].kickers[i] > h[1].kickers[i]){
                            winner = h[0];
                            break;
                        }else if (h[0].kickers[i] < h[1].kickers[i]){
                            winner = h[1];
                            break;
                        }
                    }
                    if(isEmpty(winner)){
                        tie = true;
                    }
                }else{
                    if(h[0].valuableRank > h[1].valuableRank){
                        winner = h[0];
                    }else{
                        winner = h[1];
                    }
                }
            }
            if(h[0].isStraight){
                if(h[0].startEndRanks[0] > h[1].startEndRanks[0]){
                    winner = h[0];
                }else if(h[0].startEndRanks[0] < h[1].startEndRanks[0]){
                    winner = h[1];
                }else{
                    tie = true;
                }
            }
            if(h[0].isFourOfKind || h[0].isThreeOfKind || h[0].isHighCard){
                if(h[0].valuableRank > h[1].valuableRank){
                    winner = h[0];
                }else if (h[0].valuableRank < h[1].valuableRank){
                    winner = h[1];
                }else{
                    for(var i = 0; i < h[0].kickers.length; i++){
                        if(h[0].kickers[i] > h[1].kickers[i]){
                            winner = h[0];
                            break;
                        }else if (h[0].kickers[i] < h[1].kickers[i]){
                            winner = h[1];
                            break;
                        }
                    }
                    if(isEmpty(winner)){
                        tie = true;
                    }
                }
            }
            if(h[0].isTwoPairs || h[0].isPair){
                for(var i = 0; i < h[0].pairs.length; i++){
                    if(h[0].pairs[i] > h[1].pairs[i]){
                        winner = h[0];
                        break;
                    }else if (h[0].pairs[i] < h[1].pairs[i]){
                        winner = h[1];
                        break;
                    }
                }
                if(isEmpty(winner)){
                    for(var i = 0; i < h[0].kickers.length; i++){
                        if(h[0].kickers[i] > h[1].kickers[i]){
                            winner = h[0];
                            break;
                        }else if (h[0].kickers[i] < h[1].kickers[i]){
                            winner = h[1];
                            break;
                        }
                    }
                    if(isEmpty(winner)){
                        tie = true;
                    }
                }
            }
            if(h[0].isFullHouse){
                for(var i = 0; i < 2; i++){
                    if(h[0].startEndRanks[i] > h[1].startEndRanks[i]){
                        winner = h[0];
                        break;
                    }else if(h[0].startEndRanks[i] < h[1].startEndRanks[i]){
                        winner = h[1];
                        break;
                    }else{
                        tie = true;
                    }
                }
            }
        }else{
            winner = h[0];
        }

        if(tie == true) {
            winner = h[0];
            winner.resultLiteral = 'Split Pot HI (' + h[0].strengthHighLiteral + ')';
        }else{
            winner.resultLiteral = winner.player + ' wins HI (' + winner.strengthHighLiteral + ')';
        }

        return winner;
    }

    /**
     * Determin which of two hands is stronger for Low 8 Hand. Receive hands after they passed through {Evaluator}
     * <br>Alghoritm:
     * <br>1. Check if both hands not qualify for Low Hand
     * <br>2. If {1} is True then both hands are unqualified
     * <br>3. Otherwise check if first hand qualify Low 8 Hand and second not
     * <br>4. If {3} True, first hand is winner
     * <br>5. Otherwise check if second hand qualify for Low 8 Hand and first not
     * <br>6. If {5} true, second hand is winner
     * <br>7. If all conditions above are false, both hand qualify and we determine winner by looping through ranks and
     * searching for first missmatch when one hand card will be bigger. If found, hand with lower found card rank win.
     * <br>8. If both hands are identical it's a tie
     * <br>9. Finally, compute results into human friendly resultLiteral string and return winning hand. If it's tie,
     * proper resultLiteral string created and first hand set as winner for data consistency.
     * @param hand1 {{}} HandA Object for comparing
     * @param hand2 {{}} HandB Object for comparing
     * @returns {{}} Winner Hand Object
     */
    this.compareLowHands = function(hand1, hand2){
        var h = [hand1, hand2];
        var winner = {};
        var tie = false;
        var unqualified = false;

        if(h[0].isLowHand === false && h[1].isLowHand === false){
            unqualified = true;
        }else if(h[0].isLowHand === true && h[1].isLowHand === false){
            winner = h[0];
        }else if(h[0].isLowHand === false && h[1].isLowHand === true){
            winner = h[1];
        }else{
            for(var rank = 0; rank < h[0].ranks.length; rank++){
                if(h[0].ranks[rank] != h[1].ranks[rank]){
                    Math.min(h[0].ranks[rank], h[1].ranks[rank]) == h[0].ranks[rank] ? winner = h[0] : winner = h[1];
                    break;
                }
            }
            if(isEmpty(winner)){
                tie = true;
            }
        }

        if(tie === true) {
            winner = h[0];
            winner.resultLiteral = 'Split Pot Lo (' + winner.cardsRawLow + ')';
        }else if (unqualified) {
            winner = h[0];
            winner.resultLiteral = 'No hand qualified for Low';
        }else{
            winner.resultLiteral = winner.player + ' wins Lo (' + winner.cardsRawLow+ ')';
        }

        return winner;
    }
}