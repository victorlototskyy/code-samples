/**
 * Evaluate strength of single hand based on Omaha-Hi-Low poker rules.
 * @Class
 * @param hand {Array} Array of 5 cards
 * @param type {high|low} Type of hand
 * @param player {String} Player name String
 * @constructor
 */
var Evaluator = function (hand, type, player) { /** @lends Evaluator */

    /**
     * @desc Contain initial array with 5 hand cards
     * @type {Array}
     */
    this.hand = hand;

    /**
     * @desc Define if class should evaluate High Hand or Low 8 Hand strength
     * @type {high|low}
     */
    this.type = type;

    /**
     * @desc Player name which will be used for outputting results
     * @type {String}
     */
    this.player = player;

    /**
     * @desc Contain array with hand cards which were parsed by Evaluator
     * @type {Array}
     */
    this.cards = [];

    /**
     * @desc Contain array with hand cards ranks
     * @type {Array}
     */
    this.ranks = [];

    /**
     * @desc Contain array with hand cards suits
     * @type {Array}
     */
    this.suits = [];

    /**
     * @desc Contain string notation for Low 8 hand results output
     * @type {String}
     * @example 6432A, 5432A
     */
    this.cardsRawLow = '';

    /**
     * @desc Cards ranks sorted by strength that used to determine winning hand in particular situations during
     * comparison at {Comparator}.
     * @type {Array}
     * @example If we need to compare two Full House combinations:
     * HandA: KcKhKd8c8d - startEndRanks[13, 8]
     * HandB: QcQhQd8c8d - startEndRanks[12, 8]
     */
    this.startEndRanks = [];

    /**
     * @desc Cards pairs ranks sorted by strength that used to determine winnign hand in partuclar situations
     * during comparison at {Comparator}.
     * @type {Array}
     * @example If we need to compare two TwoPairs combinations;
     * HandA: KcKh9c9d3d - pairs[13,9]
     * HandB: JcJh5c5d3d - pairs[11,5]
     */
    this.pairs = [];

    /**
     * @desc Hand kicker ranks sorted by strength and used to determine winning hand in particular situations
     * during comparison at {Comparator}.
     * @type {Array}
     * @example If we need to compare two TwoPairs combinations;
     * HandA: KcKh9c9d3d - kickers[3]
     * HandB: KcKh9c9d5d - kickers[5]
     */
    this.kickers = [];

    /**
     * @desc In particular hand combinations, contain card rank which can be used to determine winner if
     * both hands combinations are same.
     * @type {Integer}
     * @example If we need to compare two FourOfKind hands:
     * HandA: KcKhKsKd5d - valuableRank[5]
     * HandB: KcKhKsKd9d - valuableRank[9]
     */
    this.valuableRank = -1;

    /**
     * @desc Contain string with human readable hand strength which is used at results output
     * @type {string}
     */
    this.strengthHighLiteral = '';

    /**
     * @desc Contain numerical hand strength which is used in hands comparison at {Comparator}
     * @type {number}
     */
    this.strength = 0;


    /**
     * @desc Does hand have HighCard combination
     * @type {boolean}
     */
    this.isHighCard = false;

    /**
     * @desc Does hand have Pair combination
     * @type {boolean}
     */
    this.isPair = false;

    /**
     * @desc Does hand have TwoPairs combination
     * @type {boolean}
     */
    this.isTwoPairs = false;

    /**
     * @desc Does hand have ThreeOfKind combination
     * @type {boolean}
     */
    this.isThreeOfKind = false;

    /**
     * @desc Does hand have Straight combination
     * @type {boolean}
     */
    this.isStraight = false;

    /**
     * @desc Does hand have StraightFlush combination
     * @type {boolean}
     */
    this.isStraightFlush = false;

    /**
     * @desc Does hand have Flush combination
     * @type {boolean}
     */
    this.isFlush = false;

    /**
     * @desc Does hand have FullHouse combination
     * @type {boolean}
     */
    this.isFullHouse = false;

    /**
     * @desc Does hand have FourOfKind combination
     * @type {boolean}
     */
    this.isFourOfKind = false;

    /**
     * @desc Does hand qualify for Low Hand acccording to Omaha-Low-High rules
     * @type {boolean}
     * @example
     * Following hand qualify: [8,6,4,4,3] - ranks strength
     * Following hand does not qualify: [J,6,4,4,3] - ranks strength
     */
    this.isLowHand = false;

    /**
     * Use incoming hand data and parse it to fill in {Evaluator} hand properties for further evaluation. Low 8 Hand
     * exception added which replace Ace rank from 14 to 1. At the end sort hand by ranks strength for further
     * evaluation.
     * @private
     */
    this._parseHand = function(){
        for(var i = 0; i < this.hand.length; i++){
            var card = this.hand[i];
            this.cards.push(card);
            this.suits.push(card.suit);
            (this.type == 'low' && card.rank == 14) ? this.ranks.push(1) : this.ranks.push(card.rank);
        }

        this.ranks.sort(function(a,b){
            return b - a;
        });
    }

    /**
     * Find duplicates in incoming array, used to determine either ranks or suits duplicates for various combinations
     * evaluation.
     * @param ranksOrSuits {Array} of ranks or suits
     * @returns {Array} array of unique values from incoming array
     * @private
     * @example
     * Incoming: ranks[3,6,3,6,2] - function return [0,1,2]
     */
    this._countDuplicates = function(ranksOrSuits){
        var counts = {};
        for(var i = 0; i < ranksOrSuits.length; i++) {
            counts["" + ranksOrSuits[i]] = (counts[ranksOrSuits[i]] || 0) + 1;
        }
        return counts;
    }

    /**
     * Determine if hand has StraightFlush which is highest combinations in Omaha-Low-High and to achieve it we
     * check if hand evaluate to Flush and Straight.
     * @returns {boolean} True if hand evaluate to combination
     * @private
     */
    this._isHandStraightFlush = function(){
        var result = false;

        if(this._isHandFlush() === true && this._isHandStraight() === true){
            this.isStraightFlush = true;
            this.isFlush = false;
            this.isStraight = false;
        }

        (this.isStraightFlush) ? result = true : '';
        return result;
    }

    /**
     * Determine if hand has Straight combination, alghoritm:
     * 1. Check if all cards ranks are unique
     * 2. If {1} true, loop through ranks. In each rank iteration check if previous rank is weaker by 1 point.
     * If it is, we set Straight combination to True, if not, set to false and exit from loop.
     * 3. If {2} is false, check for unique combination with Ace as first in Straight. If array match, set Straight
     * combination to true and re-arrange hand ranks for proper comparison.
     * 4. If combination is Straight, fill startEndRanks propery with first and last cards ranks in the hand for
     * further comparison.
     * @returns {boolean} True if hand evaluate to combination
     * @private
     */
    this._isHandStraight = function(){
        var result = false;
        if(Object.keys(this._countDuplicates(this.ranks)).length == 5){
            var start = this.ranks[0];
            for(var i = 1; i < this.ranks.length; i++){
                if(this.ranks[i] == start - 1){
                    this.isStraight = true;
                    start -= 1;
                }else{
                    this.isStraight = false;
                    break;
                }
            }

            if(isEmpty(this.ranks.arrayDiff([14,5,4,3,2]))){
                this.isStraight = true;
                this.ranks = [5,4,3,2,1];
            }

            if(this.isStraight){
                this.startEndRanks[0] = Deck.getValueForRank(this.ranks[0]);
                this.startEndRanks[1] = Deck.getValueForRank(this.ranks[4]);
            }

        }
        (this.isStraight) ? result = true : '';
        return result;
    }

    /**
     * Determine if hand has Flush combination, alghoritm:
     * 1. Check if all suits in hand are same.
     * 2. If {1} is True, we check if first card is Ace and fifth card is 2
     * 2.1 If {2} is True, set variableRank to second card in hand (2)
     * 2.2 If {2} is False, set variable to first card in hand
     * 3. After we add card ranks to kickers array for further comparison
     * @returns {boolean} True if hand evaluate to combination
     * @private
     */
    this._isHandFlush = function(){
        var result = false;

        if(Object.keys(this._countDuplicates(this.suits)).length == 1){
            this.isFlush = true;
            this.isHighCard = false;
            if(this.ranks[0] == 14 && this.ranks[4] == 2 && this.isStraight){
                this.valuableRank = Deck.getValueForRank(this.ranks[1]);
            }else{
                this.valuableRank = Deck.getValueForRank(this.ranks[0]);
            }

            this.kickers = [];
            for (var i = 1; i < this.ranks.length; i++) {
                this.kickers.push(Deck.getValueForRank(this.ranks[i]));
            }
        }

        (this.isFlush) ? result = true : '';
        return result;
    }

    /**
     * Determine if hand has HighCard combination, alghoritm:
     * 1. Check if all card ranks are unique
     * 2. Add card ranks to kicker property for further comparison
     * 3. Set valuableRank property to card with biggest rank for further comparison
     * 4. Even {1} is same determining Straight combination, if hand has Straight, HighCard evaluation will not be
     * triggered as Straight evaluation goes first.
     * @returns {boolean} True if hand evaluate to combination
     * @private
     */
    this._isHandHighCard = function(){
        var result = false;

        if(Object.keys(this._countDuplicates(this.ranks)).length == 5) {
            this.isHighCard = true;
            for (var i = 0; i < this.ranks.length; i++) {
                this.kickers.push(Deck.getValueForRank(this.ranks[i]));
            }
            this.valuableRank = this.kickers.shift();
        }

        (this.isHighCard) ? result = true : '';
        return result;
    }

    /**
     * Determine if hand has HandFourOfKind combination, alghoritm:
     * 1. Check if there are 2 unique ranks in hand
     * 2. If {1} is True, loop through ranks and check if there are 4 cards with same rank
     * 3. If {2} is True, set valuableRank property with one of 4-cards rank and add remaining card as kicker
     * @returns {boolean} True if hand evaluate to combination
     * @private
     */
    this._isHandFourOfKind = function(){
        var result = false;

        if(Object.keys(this._countDuplicates(this.ranks)).length == 2){
            for(var rank in this._countDuplicates(this.ranks)){
                if(this._countDuplicates(this.ranks)[rank] == 4){
                    this.isFourOfKind = true;
                    this.valuableRank = Deck.getValueForRank(rank);
                    for(var rank in this.ranks){
                        if(this.ranks[rank] != this.valuableRank){
                            this.kickers.push(this.ranks[rank]);
                        }
                    }
                }
            }
        }

        (this.isFourOfKind) ? result = true : '';
        return result;
    }

    /**
     * Determine if hand has FullHouse combination, alghoritm:
     * 1. Check if there are 2 unique ranks in hand
     * 2. If {1} is True, check if one of ranks had 3 cards and other 2 cards to match combination requirements
     * 3. 3-cards rank added first to startEndRanks and 2-cards rank added second to match rules during evaluation
     * @returns {boolean} True if hand evaluate to combination
     * @private
     */
    this._isHandFullHouse = function(){
        var result = false;

        if(Object.keys(this._countDuplicates(this.ranks)).length == 2){
            for(var rank in this._countDuplicates(this.ranks)){
                if(this._countDuplicates(this.ranks)[rank] == 3){
                    this.isFullHouse = true;
                    this.startEndRanks[0] = Deck.getValueForRank(rank)
                }
                if(this._countDuplicates(this.ranks)[rank] == 2){
                    this.startEndRanks[1] = Deck.getValueForRank(rank)
                }
            }
        }

        (this.isFullHouse) ? result = true : '';
        return result;
    }

    /**
     * Determine if hand has ThreeOfKind combination, alghoritm:
     * 1. Check if hand has 3 different ranks. Even 2 or 1 would satisfy ThreeOfKind requirements, having 2 or 1
     * unique ranks would result in higher combination which will be evaluated before ThreeOfKind, so this method
     * will not be triggered.
     * 2. If {1} is True, loop through ranks and check if there are 3 cards of same rank
     * 3. If {2} is True, set 3-cards rank as valuableRank property and remaining cards as kickers for further
     * comparison.
     * @returns {boolean} True if hand evaluate to combination
     * @private
     */
    this._isHandThreeOfKind = function(){
        var result = false;
        var self = this;

        if(Object.keys(this._countDuplicates(this.ranks)).length == 3){
            var ranks = Object.keys(this._countDuplicates(this.ranks)).map(function(rank){
                return self._countDuplicates(self.ranks)[rank];
            });
            for(var key in ranks){
                if(ranks[key] == 3){
                    self.isThreeOfKind = true;
                    for(var item in this._countDuplicates(this.ranks)){
                        if(self._countDuplicates(self.ranks)[item] == 3){
                            self.valuableRank = Deck.getValueForRank(item);
                        }
                    }
                    for(var rank in self.ranks){
                        if(self.ranks[rank] != self.valuableRank){
                            self.kickers.push(self.ranks[rank]);
                        }
                    }
                }
            }
        }

        (this.isThreeOfKind) ? result = true : '';
        return result;
    }

    /**
     * Determine if hand has TwoPairs combination, alghoritm:
     * 1. Check if hand has 3 different ranks. Even 2 or 1 would satisfy TwoPairs requirements, having 2 or 1
     * unique ranks would result in higher combination which will be evaluated before TwoPairs, so this method
     * will not be triggered.
     * 2. If {1} is True, loop through ranks to check if there are 2 unique pairs with same rank
     * 3. If {2} is True, unique pairs ranks values pushed to pairs property and remaining card pushed to kicker
     * property for further comparison.
     * @returns {boolean} True if hand evaluate to combination
     * @private
     */
    this._isHandTwoPairs = function(){
        var result = false;
        var self = this;

        if(Object.keys(this._countDuplicates(this.ranks)).length == 3){
            var ranks = Object.keys(this._countDuplicates(this.ranks)).map(function(rank){
                return self._countDuplicates(self.ranks)[rank];
            });
            for(var key in ranks){
                if(ranks[key] == 2 && !self.pairs.length){
                    this.isTwoPairs = true;
                    for(var item in this._countDuplicates(this.ranks)){
                        if(self._countDuplicates(self.ranks)[item] == 2){
                            self.pairs.push(Deck.getValueForRank(item));
                        }
                    }

                    for(var rank in self.ranks){
                        if(self.ranks[rank] != self.pairs[0] && self.ranks[rank] != self.pairs[1]
                            && self.kickers.indexOf(self.ranks[rank]) < 0){
                            self.kickers.push(self.ranks[rank]);
                        }
                    }
                }
            }
        }

        (this.isTwoPairs) ? result = true : '';
        return result;
    }

    /**
     * Determine if hand has OnePair combination, alghoritm:
     * 1. Check if hand has 4 different ranks
     * 2. If {1} is true, loop through ranks to check if there is pair to match combination requirements
     * 3. If {2} is true, unique rank from pair combination added to pair property and remaining cards added to
     * kickers property for further comparison.
     * @returns {boolean} True if hand evaluate to combination
     * @private
     */
    this._isHandPair = function(){
        var result = false;
        var self = this;

        if(Object.keys(this._countDuplicates(this.ranks)).length == 4){
            var ranks = Object.keys(this._countDuplicates(this.ranks)).map(function(rank){
                return self._countDuplicates(self.ranks)[rank];
            });
            for(var key in ranks){
                if(ranks[key] == 2){
                    this.isPair = true;
                    for(var item in this._countDuplicates(this.ranks)){
                        if(self._countDuplicates(self.ranks)[item] == 2){
                            self.pairs.push(Deck.getValueForRank(item));
                        }
                    }
                }
            }
            for(var rank in self.ranks){
                if(self.ranks[rank] != self.pairs[0] && self.kickers.indexOf(self.ranks[rank]) < 0){
                    self.kickers.push(self.ranks[rank]);
                }
            }
        }

        (this.isPair) ? result = true : '';
        return result;
    }

    /**
     * Function for High Hand evaluation which check for possible combinations staring from highest to lowest.
     * If combination match, it set to true and function stop executing. This way, even some lower combinations
     * satisfy to requirements of higher combination and player has higher combination, lower will not be triggered
     * and hand strength will be properly calculated.
     * @example If user FourOfKind it also satisfy to ThreeOfKind, TwoPairs and Pair combinations, but their
     * evaluation will not be triggered due to evaluation order.
     * @private
     */
    this._evaluateHigh = function(){
        if(this._isHandStraightFlush()) return;
        if(this._isHandFourOfKind()) return;
        if(this._isHandFullHouse()) return;
        if(this._isHandFlush()) return;
        if(this._isHandStraight()) return;
        if(this._isHandHighCard()) return;
        if(this._isHandThreeOfKind()) return;
        if(this._isHandTwoPairs()) return;
        if(this._isHandPair()) return;
    }

    /**
     * After evaluation done, set hand strength for further hands comparison according to Omaha-Low-High rules
     * and set human friendly string for results output.
     * @private
     */
    this._setStrengthHigh = function(){
        if(this.isStraightFlush){
            this.strength = 9;
            this.strengthHighLiteral = "Straight Flush";
        }else if(this.isFourOfKind){
            this.strength = 8;
            this.strengthHighLiteral = "4-of-a-Kind";
        }else if(this.isFullHouse){
            this.strength = 7;
            this.strengthHighLiteral = "Full House";
        }else if(this.isFlush){
            this.strength = 6;
            this.strengthHighLiteral = "Flush";
        }else if(this.isStraight){
            this.strength = 5;
            this.strengthHighLiteral = "Straight";
        }else if(this.isThreeOfKind){
            this.strength = 4;
            this.strengthHighLiteral = "3-of-a-Kind";
        }else if(this.isTwoPairs){
            this.strength = 3;
            this.strengthHighLiteral = "Two Pair";
        }else if(this.isPair){
            this.strength = 2;
            this.strengthHighLiteral = "One Pair";
        }else if(this.isHighCard){
            this.strength = 1;
            this.strengthHighLiteral = "High Card";
        }
    }

    /**
     * Check if hand evaluate to Low 8 combination, alghoritm:
     * 1. Check if all ranks are unique and if first rank is not bigger than 8
     * 2. Because for Low 8 we replaced Ace rank to 1 and ranks are sorted in descending order, this check is accurate
     * 3. If {1}, set cardsRawLow property with human friendly string for data output and replace Ace rank 1 with A
     * notation.
     * @private
     */
    this._evaluateLow = function(){
        if(Object.keys(this._countDuplicates(this.ranks)).length == 5 && this.ranks[0] <= 8){
            this.cardsRawLow = this.ranks.join('').replace(/1/, 'A');
            this.isLowHand = true;
        }
    }

    /**
     * Parse incoming hand into class properties required for evaluation
     */
    this._parseHand();

    /**
     * If High Hand evaluation needed, run it, otherwise - run Low 8
     */
    if(this.type == "high"){
        this._evaluateHigh();
        this._setStrengthHigh();
    }else{
        this._evaluateLow();
    }
}