/**
 * @Class
 * @constructor
 */
var Table = function () {/** @lends Table */

    /**
     * Get input lines from text area
     * @param selector {String} contain HTML id of text area
     * @returns lines_array Array with each table cards combination
     */
    this.getInputLines = function (selector) {
        var lines = document.getElementById(selector).value.trim();
        var lines_array = lines.split("\n");
        return lines_array;
    };

    /**
     * Get list of players {Card} objects
     * @param line {String} contain single table combination
     * @param handIdentifier {A|B} contain type of player
     * @returns cardObjects {Array} Array of {Card} objects
     */
    this.getHandCards = function (line, handIdentifier) {
        var cardObjects = [];
        var regex = new RegExp("Hand"+handIdentifier+":([\\w\\d\\-]+)\\s", "gi");
        var cards = regex.exec(line.trim())[1].split("-");
        for(var i = 0; i <  cards.length; i++){
            cardObjects.push(new Card(cards[i]));
        }
        return cardObjects;
    };

    /**
     * Get list of board {Card} objects
     * @param line {String} contain single table combination
     * @returns cardObjects {Array} Array of {Card} objects
     */
    this.getBoardCards = function (line) {
        var cardObjects = [];
        var regex = new RegExp("Board:([\\w\\d\\-]+)", "gi");
        var cards = regex.exec(line.trim())[1].split("-");
        for(var i = 0; i <  cards.length; i++){
            cardObjects.push(new Card(cards[i]));
        }
        return cardObjects;
    };

}