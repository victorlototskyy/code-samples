/**
 * @Class
 * @param input {String} Contain card notation
 * @returns {Card} Instance Instance of this class
 * @constructor
 */
var Card = function(input) { /** @lends Card */
    this.suit = -1;
    this.rank = -1;
    this.twoCharacterNotation = input;

    /**
     * Create {Card} object from {Deck} based on notation
     * @param twoCharacterNotation {String} containing card notation
     * @private
     */
    this._create = function(){
        if(this.twoCharacterNotation && this.twoCharacterNotation.length == 2) {
            this.rank = Deck.getValueForRank(this.twoCharacterNotation[0]);
            this.suit = this.twoCharacterNotation[1];
        }
    }

    return this._create();
}