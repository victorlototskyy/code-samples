var table = new Table();
var comparator = new Comparator();

bindEvent(document.getElementById("compareButton"), 'click', function () {
    var lines = table.getInputLines("hands");
    var resultsContainer = document.getElementById("results");
    resultsContainer.value = '';

    // Loop through lines each of which contain set of cards on table
    for(var lines_count = 0; lines_count < lines.length; lines_count++){
        var line = lines[lines_count];

        // Get cards on board
        var board = table.getBoardCards(line);

        // Generate cards for HandA from HandA and Board cards
        var handA = new Hand(table.getHandCards(line, "A"), board, "HandA");

        // Get 5-cards permutations list from HandA and Board
        var handACombinations = handA.getCombinations();

        // Get HandA High Hand with best combination from permutations.
        var handAHighHand = handA.getHighHand(handACombinations);

        // Get HandA Low 8 Hand with best combination from permutations.
        var handALowHand = handA.getLowHand(handACombinations);

        // Generate cards for HandB from HandB and Board cards
        var handB = new Hand(table.getHandCards(line, "B"), board, "HandB");

        // Get 5-cards permutations list from HandB and Board
        var handBCombinations = handB.getCombinations();

        // Get HandB High hand with best combination from permutations
        var handBHighHand = handB.getHighHand(handBCombinations);

        // Get HandB Low 8 Hand with best combinations from permutations
        var handBLowHand = handB.getLowHand(handBCombinations);

        // Determine High Hand winner among both Hands
        var highWinnerHand = comparator.compareHighHands(handAHighHand, handBHighHand);

        // Determine Low 8 Hand winner among both Hands
        var lowWinnerHand = comparator.compareLowHands(handALowHand, handBLowHand);

        // Output results into text area
        resultsContainer.value += (line.trim())+'\n';
        resultsContainer.value += '=> '+highWinnerHand.resultLiteral+'; '+lowWinnerHand.resultLiteral+'\n\n';
    }
});



