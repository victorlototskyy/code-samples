<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">

<script type="text/javascript"> <!--empty script IE white flash fix--> </script>
<link href="/css/reset.css" rel="stylesheet" type="text/css" />
<link href="/css/base.css" rel="stylesheet" type="text/css" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />


<!-- styles needed by jScrollPane -->
<link type="text/css" href="/scrollpane/jquery.jscrollpane.css" rel="stylesheet" media="all" />
<link type="text/css" href="/scrollpane/jquery.jscrollpane.lozenge.css" rel="stylesheet" media="all" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<!-- the mousewheel plugin -->
<script type="text/javascript" src="/scrollpane/jquery.mousewheel.js"></script>
<!-- the jScrollPane script -->
<script type="text/javascript" src="/scrollpane/jquery.jscrollpane.min.js"></script>

<script src="/cufon/cufon-yui.js" type="text/javascript"></script>
<script src="/cufon/Helvetica_Neue_LT_Std_300-Helvetica_Neue_LT_Std_700.font.js" type="text/javascript"></script>  
<script src="/js/cufreplace.js" type="text/javascript"></script> 

<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/slides.js" ></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" src="/js/transition.js"></script>

<script type="text/javascript">
		$(document).ready(function() {

			$("a[rel]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});

		});
	</script>
