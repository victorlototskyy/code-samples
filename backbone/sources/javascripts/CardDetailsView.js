var CardDetailsView = Backbone.View.extend({
	template: _.template($('#tpl-details').html()),
	render: function() {
		this.$el.html(this.template(this.model.serialize()));
		return this;
	}
});