var CardControlsReversePrintView = CardControlsView.extend({
	template: _.template($('#tpl-controls-reverse-print').html()),
	initialize: function() {
		this.listenTo(this.model, 'change:reversePrint', this.render);
	}
});