var CardControlsLayoutView = CardControlsView.extend({
	template: _.template($('#tpl-controls-layout').html()),
	initialize: function() {
		this.listenTo(this.model, 'change:layout', this.render);
	}
});