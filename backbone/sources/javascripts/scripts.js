// @codekit-prepend "Wizard.js";
// @codekit-prepend "Step.js";
// @codekit-prepend "StepView.js";
// @codekit-prepend "Card.js";
// @codekit-prepend "CardView.js";
// @codekit-prepend "CardFlipView.js";
// @codekit-prepend "CardControlsView.js";
// @codekit-prepend "CardControlsColorView.js";
// @codekit-prepend "CardControlsLayoutView.js";
// @codekit-prepend "CardControlsPrintView.js";
// @codekit-prepend "CardControlsWordingView.js";
// @codekit-prepend "CardControlsReverseLayoutView.js";
// @codekit-prepend "CardControlsReversePrintView.js";
// @codekit-prepend "CardControlsReverseWordingView.js";
// @codekit-prepend "CardControlsUploadView.js";
// @codekit-prepend "CardDetailsView.js";
// @codekit-prepend "cardOptions.js";

Dropzone.autoDiscover = false;

$(document).ready(function(){
	
	window.card = new Card();
	var cardView = new CardView({model:card});
	var cardFlipView = new CardFlipView({model:card});
	var cardControlsColorView = new CardControlsColorView({model:card});
	var cardControlsLayoutView = new CardControlsLayoutView({model:card});
	var cardControlsPrintView = new CardControlsPrintView({model:card});
	var cardControlsWordingView = new CardControlsWordingView({model:card});
	var cardControlsReverseLayoutView = new CardControlsReverseLayoutView({model:card});
	var cardControlsReversePrintView = new CardControlsReversePrintView({model:card});
	var cardControlsReverseWordingView = new CardControlsReverseWordingView({model:card});
	var cardControlsUploadView = new CardControlsUploadView({model:card});
	var cardDetailsView = new CardDetailsView({model:card});

	var cardSteps = [{
		title: 'Choose your card colour',
		slug: 'color',
		content: [{
			renderTo: 'card',
			view: cardView
		},{
			renderTo: 'controls',
			view: cardControlsColorView
		}],
		navigation: {
			'Next': 'front'
		}
	},{
		title: 'Choose your front layout',
		slug: 'front',
		content: [{
			renderTo: 'card',
			view: cardView
		},{
			renderTo: 'controls',
			view: cardControlsLayoutView
		}],
		navigation: {
			'Back': 'color',
			'Next': 'print'
		}
	},{
		title: 'Choose your print colour',
		slug: 'print',
		content: [{
			renderTo: 'card',
			view: cardView
		},{
			renderTo: 'controls',
			view: cardControlsPrintView
		}],
		navigation: {
			'Back': 'front',
			'Next': 'wording'
		}
	},{
		title: 'Type your wording',
		slug: 'wording',
		content: [{
			renderTo: 'card',
			view: cardView
		},{
			renderTo: 'controls',
			view: cardControlsWordingView
		}],
		navigation: {
			'Back': 'print',
			'Next': 'reverse'
		},
		conditions: [{
			listenTo: card,
			value: 'color',
			execute: 'replaceNavigationForWording'
		}]
	},{
		title: 'Choose your reverse layout',
		slug: 'reverse',
		content: [{
			renderTo: 'card',
			view: cardView,
			options: {
				startWithReverse: true
			}
		},{
			renderTo: 'controls',
			view: cardControlsReverseLayoutView
		}],
		navigation: {
			'Back': 'wording',
			'Next': 'logo'
		},
		conditions: [{
			listenTo: card,
			value: 'hasReverse',
			execute: 'replaceNavigationForReverse'
		}]
	},{
		title: 'Choose your reverse print colour',
		slug: 'reverseprint',
		content: [{
			renderTo: 'card',
			view: cardView,
			options: {
				startWithReverse: true
			}
		},{
			renderTo: 'controls',
			view: cardControlsReversePrintView
		}],
		navigation: {
			'Back': 'reverse',
			'Next': 'reversewording'
		}
	},{
		title: 'Type your wording',
		slug: 'reversewording',
		content: [{
			renderTo: 'card',
			view: cardView,
			options: {
				startWithReverse: true
			}
		},{
			renderTo: 'controls',
			view: cardControlsReverseWordingView
		}],
		navigation: {
			'Back': 'reverseprint',
			'Next': 'logo'
		}
	},{
		title: 'Upload your company logo',
		slug: 'logo',
		navigation: {
			'Back': 'reverse',
			'Next': 'done'
		},
		content: [{
			renderTo: 'controls',
			view: cardControlsUploadView
		}],
		conditions: [{
			listenTo: card,
			value: 'hasReverse',
			execute: 'replaceNavigationForLogo'
		},{
			listenTo: card,
			value: 'color',
			execute: 'replaceNavigationForNonReverseLogo'
		}]
	},{
		title: 'Are you happy with your design?',
		slug: 'done',
		navigation: {
			'No': 'wording',
			'Yes': 'details'
		},
		content: [{
			renderTo: 'card',
			view: cardFlipView
		}]
	},{
		title: 'Fill in your details for a no obligation quote',
		slug: 'details',
		content: [{
			renderTo: 'controls',
			view: cardDetailsView
		}]
	}];

	window.wizard = new Wizard($('#wizard'), cardSteps);
	Backbone.history.start();

});