var CardView = Backbone.View.extend({
  className: "card",
  template: _.template($('#tpl-card').html()),
  initialize: function() {
  	this.listenTo(this.model, "change", this.render);
  },

  events: {
    'click .card-link-show': 'showLink',
    'click .card-link-hide': 'hideLink'
  },

  showLink: function(){
    var input = this.$('.card-link-input').show().focus().get(0);
    input.setSelectionRange(0, input.value.length);
    this.$('.card-link-show').hide();
    this.$('.card-link-hide').show();
  },

  hideLink: function(){
    var input = this.$('.card-link-input').hide().get(0);
    input.setSelectionRange(0,0);
    this.$('.card-link-show').show();
    this.$('.card-link-hide').hide();
  },

  render: function(options) {
    if(_.isUndefined(options.cid)){
      _.extend(this.model.attributes, options);
    }
    this.$el.html(this.template(this.model.decodeOptions()));
    return this;
  }

});