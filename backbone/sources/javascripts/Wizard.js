var Wizard = Backbone.Router.extend({

	currentView: null,

	switchView: function(view) {
		if (this.currentView) {
			this.currentView.$el.remove();
		}
		this.$el.html(view.$el);
		view.render();
		this.currentView = view;
	},

	routes: {
		'wizard/:slug': 'showStep',
		'card/:c/:l/:p/:ttw/:ttf/:tts/:ttb/:tti/:ttu/:mtw/:mtf/:mts/:mtb/:mti/:mtu/:hbt/:btw/:btf/:bts/:btb/:bti/:btu/:swr/:hr/:rl/:rp/:rttw/:rttf/:rtts/:rttb/:rtti/:rttu/:rmtw/:rmtf/:rmts/:rmtb/:rmti/:rmtu/:rbtw/:rbtf/:rbts/:rbtb/:rbti/:rbtu/:lo': 'showCard'
	},
	
	showStep: function(slug) {
		if(this.views[slug]){
			this.switchView(this.views[slug]);
		}else{
			var emptyView = new StepView();
			this.switchView(emptyView);
		}
	},

	showCard: function(c,l,p,ttw,ttf,tts,ttb,tti,ttu,mtw,mtf,mts,mtb,mti,mtu,hbt,btw,btf,bts,btb,bti,btu,swr,hr,rl,rp,rttw,rttf,rtts,rttb,rtti,rttu,rmtw,rmtf,rmts,rmtb,rmti,rmtu,rbtw,rbtf,rbts,rbtb,rbti,rbtu,lo){
		var options = [
			'color',
			'layout',
			'print',
			'topTextWording',
			'topTextFont',
			'topTextSize',
			'topTextBold',
			'topTextItalic',
			'topTextUnderline',
			'middleTextWording',
			'middleTextFont',
			'middleTextSize',
			'middleTextBold',
			'middleTextItalic',
			'middleTextUnderline',
			'hasBottomText',
			'bottomTextWording',
			'bottomTextFont',
			'bottomTextSize',
			'bottomTextBold',
			'bottomTextItalic',
			'bottomTextUnderline',
			'startWithReverse',
			'hasReverse',
			'reverseLayout',
			'reversePrint',
			'reverseTopTextWording',
			'reverseTopTextFont',
			'reverseTopTextSize',
			'reverseTopTextBold',
			'reverseTopTextItalic',
			'reverseTopTextUnderline',
			'reverseMiddleTextWording',
			'reverseMiddleTextFont',
			'reverseMiddleTextSize',
			'reverseMiddleTextBold',
			'reverseMiddleTextItalic',
			'reverseMiddleTextUnderline',
			'reverseBottomTextWording',
			'reverseBottomTextFont',
			'reverseBottomTextSize',
			'reverseBottomTextBold',
			'reverseBottomTextItalic',
			'reverseBottomTextUnderline',
			'logo'
		];
		var data = _.object(options, arguments);
		if(data['startWithReverse'] == 'false'){
			data['startWithReverse'] = false;
		}
		if(data['startWithReverse'] == 'true'){
			data['startWithReverse'] = true;
		}
		if(data['hasReverse'] == 'false'){
			data['hasReverse'] = false;
		}
		if(data['hasReverse'] == 'true'){
			data['hasReverse'] = true;
		}
		if(data['hasBottomText'] == 'false'){
			data['hasBottomText'] = false;
		}
		if(data['hasBottomText'] == 'true'){
			data['hasBottomText'] = true;
		}
		if(data['logo'] == 'false'){
			data['logo'] = false;
		}
		if(data['logo'] == 'true'){
			data['logo'] = true;
		}

		card.set(data);
		Backbone.history.navigate('wizard/color', {trigger: true});
	},

	initialize: function(el, stepsArray) {
		var self = this;
		this.views = {};
		this.steps = new Backbone.Collection([], {
			model: Step
		});

		if(el){
			this.$el = el;
		}else{
			this.$el = $("#wizard");
		}

		if(stepsArray){
			this.steps.add(stepsArray);
			this.steps.each(function(step){
				var view = new StepView({model:step});
				self.views[step.get('slug')] = view;
			});
		}else{
			var emptyStep = new Step();
			var view = new StepView({model: emptyStep});
			self.views[emptyStep.get('slug')] = view;
		}
	}
});