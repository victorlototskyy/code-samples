var CardControlsPrintView = CardControlsView.extend({
	template: _.template($('#tpl-controls-print').html()),
	initialize: function() {
		this.listenTo(this.model, 'change:print', this.render);
	}
});