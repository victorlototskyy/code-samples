var Step = Backbone.Model.extend({
	defaults: {
		title: "Empty step",
		tpl: '#tpl-step',
		slug: 'empty',
		navigation: {}
	}
});