var StepView = Backbone.View.extend({
  className: "wizard-step",
  
  initialize: function() {
    var self = this;
  	this.template = _.template($(this.model.get('tpl')).html());
    _.each(this.model.attributes.conditions, function(condition){
      self.listenTo(condition.listenTo, 'change:' + condition.value, self[condition.execute]);
    });
  },

  render: function() {
    this.$el.html(this.template(this.model.attributes));
		_.each(this.model.attributes.content, function(item){
			item.view.setElement('#' + item.renderTo);
			var options = _.extend({
				startWithReverse: false
			}, item.options);
			item.view.render(options);
  	});
    return this;
  },

  replaceNavigationForReverse: function(model, value){
    if(value){
      this.model.set('navigation', {
        'Back': 'wording',
        'Next': 'reverseprint'
      });
    }else{
      this.model.set('navigation', {
        'Back': 'wording',
        'Next': 'logo'
      });
    }
    this.render();
  },

  replaceNavigationForLogo: function(model, value){
    if(value){
      this.model.set('navigation', {
        'Back': 'reversewording',
        'Next': 'done'
      });
    }else{
      this.model.set('navigation', {
        'Back': 'reverse',
        'Next': 'done'
      });
    }
    this.render();
  },

  replaceNavigationForNonReverseLogo: function(model, value){
    var arr = ['00b', '00f'];
    if(_.indexOf(arr, value) != -1){
      this.model.set('navigation', {
        'Back': 'wording',
        'Next': 'done'
      });
    }
  },

  replaceNavigationForWording: function(model, value){
    var arr = ['00b', '00f'];
    if(_.indexOf(arr, value) != -1){
      this.model.set('navigation', {
        'Back': 'print',
        'Next': 'logo'
      });
    }else{
      this.model.set('navigation', {
        'Back': 'print',
        'Next': 'reverse'
      });
    }
  }
});