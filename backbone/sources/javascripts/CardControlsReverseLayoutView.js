var CardControlsReverseLayoutView = CardControlsView.extend({
	template: _.template($('#tpl-controls-reverse-layout').html()),
	initialize: function() {
		this.listenTo(this.model, 'change:reverseLayout', this.render);
	}
});