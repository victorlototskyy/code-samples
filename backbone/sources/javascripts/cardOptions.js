var cardOptions = {
	// Colors
	'000': {
		type: 'color',
		name: 'White',
		value: 'card-color-white'
	},
	'001': {
		type: 'color',
		name: 'Tan',
		value: 'card-color-tan'
	},
	'002': {
		type: 'color',
		name: 'Yellow',
		value: 'card-color-yellow'
	},
	'003': {
		type: 'color',
		name: 'Orange',
		value: 'card-color-orange'
	},
	'004': {
		type: 'color',
		name: 'Red',
		value: 'card-color-red'
	},
	'005': {
		type: 'color',
		name: 'Light Blue',
		value: 'card-color-lightblue'
	},
	'006': {
		type: 'color',
		name: 'Blue',
		value: 'card-color-blue'
	},
	'007': {
		type: 'color',
		name: 'Dark Blue',
		value: 'card-color-darkblue'
	},
	'008': {
		type: 'color',
		name: 'Purple',
		value: 'card-color-purple'
	},
	'009': {
		type: 'color',
		name: 'Cranberry',
		value: 'card-color-cranberry'
	},
	'00a': {
		type: 'color',
		name: 'Pink',
		value: 'card-color-pink'
	},
	'00b': {
		type: 'color',
		name: 'Transparent',
		value: 'card-color-transparent'
	},
	'00c': {
		type: 'color',
		name: 'Green',
		value: 'card-color-green'
	},
	'00d': {
		type: 'color',
		name: 'Dark Green',
		value: 'card-color-darkgreen'
	},
	'00e': {
		type: 'color',
		name: 'Gloss Black',
		value: 'card-color-glossblack'
	},
	'00f': {
		type: 'color',
		name: 'Frosted Clear',
		value: 'card-color-frosted'
	},
	'010': {
		type: 'color',
		name: 'Bronze',
		value: 'card-color-bronze'
	},
	'011': {
		type: 'color',
		name: 'Silver',
		value: 'card-color-silver'
	},
	'012': {
		type: 'color',
		name: 'Gold',
		value: 'card-color-gold'
	},
	'013': {
		type: 'color',
		name: 'Matt Black',
		value: 'card-color-mattblack'
	},

	// Layouts
	'100': {
		type: 'layout',
		name: 'Business card 1',
		value: 'card-layout-1'
	},
	'101': {
		type: 'layout',
		name: 'Business card 2',
		value: 'card-layout-2'
	},
	'102': {
		type: 'layout',
		name: 'Business card 3',
		value: 'card-layout-3'
	},
	'103': {
		type: 'layout',
		name: 'Business card 4',
		value: 'card-layout-4'
	},
	'104': {
		type: 'layout',
		name: 'Business card 5',
		value: 'card-layout-5'
	},
	'105': {
		type: 'layout',
		name: 'Business card 6',
		value: 'card-layout-6'
	},
	'106': {
		type: 'layout',
		name: 'Business card 7',
		value: 'card-layout-7'
	},
	'107': {
		type: 'layout',
		name: 'Privilege card',
		value: 'card-layout-8'
	},
	'108': {
		type: 'layout',
		name: 'Membership card',
		value: 'card-layout-9'
	},
	'109': {
		type: 'layout',
		name: 'Identity card',
		value: 'card-layout-10'
	},
	'10a': {
		type: 'reverseLayout',
		name: 'Blank reverse card',
		value: 'card-reverse-layout-1'
	},
	'10b': {
		type: 'reverseLayout',
		name: 'Reverse card 1',
		value: 'card-reverse-layout-2'
	},
	'10c': {
		type: 'reverseLayout',
		name: 'Calendar',
		value: 'card-reverse-layout-3'
	},
	'10d': {
		type: 'reverseLayout',
		name: 'One signature',
		value: 'card-reverse-layout-4'
	},
	'10e': {
		type: 'reverseLayout',
		name: 'Two signatures',
		value: 'card-reverse-layout-5'
	},
	'10f': {
		type: 'reverseLayout',
		name: 'Text and signature',
		value: 'card-reverse-layout-6'
	},
	'110': {
		type: 'reverseLayout',
		name: 'Text and two signatures',
		value: 'card-reverse-layout-7'
	},

	// Print colors
	'200': {
		type: 'print',
		name: 'White',
		value: 'card-print-white'
	},
	'201': {
		type: 'print',
		name: 'Orange',
		value: 'card-print-orange'
	},
	'202': {
		type: 'print',
		name: 'Yellow',
		value: 'card-print-yellow'
	},
	'203': {
		type: 'print',
		name: 'Light Green',
		value: 'card-print-lightgreen'
	},
	'204': {
		type: 'print',
		name: 'Light Blue',
		value: 'card-print-lightblue'
	},
	'205': {
		type: 'print',
		name: 'Green',
		value: 'card-print-green'
	},
	'206': {
		type: 'print',
		name: 'Mid Blue',
		value: 'card-print-midblue'
	},
	'207': {
		type: 'print',
		name: 'Dark Green',
		value: 'card-print-darkgreen'
	},
	'208': {
		type: 'print',
		name: 'Royal Blue',
		value: 'card-print-royalblue'
	},
	'209': {
		type: 'print',
		name: 'Black',
		value: 'card-print-black'
	},
	'20a': {
		type: 'print',
		name: 'Dark Blue',
		value: 'card-print-darkblue'
	},
	'20b': {
		type: 'print',
		name: 'Grey',
		value: 'card-print-grey'
	},
	'20c': {
		type: 'print',
		name: 'Purple',
		value: 'card-print-purple'
	},
	'20d': {
		type: 'print',
		name: 'Metallic Magenta',
		value: 'card-print-metallicmagenta'
	},
	'20e': {
		type: 'print',
		name: 'Red',
		value: 'card-print-red'
	},
	'20f': {
		type: 'print',
		name: 'Metallic Copper',
		value: 'card-print-metalliccopper'
	},
	'210': {
		type: 'print',
		name: 'Burgundy',
		value: 'card-print-burgundy'
	},
	'211': {
		type: 'print',
		name: 'Metallic Silver',
		value: 'card-print-metallicsilver'
	},
	'212': {
		type: 'print',
		name: 'Pink',
		value: 'card-print-pink'
	},
	'213': {
		type: 'print',
		name: 'Metallic Gold',
		value: 'card-print-metallicgold'
	},
	// --
	'214': {
		type: 'reversePrint',
		name: 'White',
		value: 'card-print-white'
	},
	'215': {
		type: 'reversePrint',
		name: 'Orange',
		value: 'card-print-orange'
	},
	'216': {
		type: 'reversePrint',
		name: 'Yellow',
		value: 'card-print-yellow'
	},
	'217': {
		type: 'reversePrint',
		name: 'Light Green',
		value: 'card-print-lightgreen'
	},
	'218': {
		type: 'reversePrint',
		name: 'Light Blue',
		value: 'card-print-lightblue'
	},
	'219': {
		type: 'reversePrint',
		name: 'Green',
		value: 'card-print-green'
	},
	'21a': {
		type: 'reversePrint',
		name: 'Mid Blue',
		value: 'card-print-midblue'
	},
	'21b': {
		type: 'reversePrint',
		name: 'Dark Green',
		value: 'card-print-darkgreen'
	},
	'21c': {
		type: 'reversePrint',
		name: 'Royal Blue',
		value: 'card-print-royalblue'
	},
	'21d': {
		type: 'reversePrint',
		name: 'Black',
		value: 'card-print-black'
	},
	'21e': {
		type: 'reversePrint',
		name: 'Dark Blue',
		value: 'card-print-darkblue'
	},
	'21f': {
		type: 'reversePrint',
		name: 'Grey',
		value: 'card-print-grey'
	},
	'220': {
		type: 'reversePrint',
		name: 'Purple',
		value: 'card-print-purple'
	},
	'221': {
		type: 'reversePrint',
		name: 'Metallic Magenta',
		value: 'card-print-metallicmagenta'
	},
	'222': {
		type: 'reversePrint',
		name: 'Red',
		value: 'card-print-red'
	},
	'223': {
		type: 'reversePrint',
		name: 'Metallic Copper',
		value: 'card-print-metalliccopper'
	},
	'224': {
		type: 'reversePrint',
		name: 'Burgundy',
		value: 'card-print-burgundy'
	},
	'225': {
		type: 'reversePrint',
		name: 'Metallic Silver',
		value: 'card-print-metallicsilver'
	},
	'226': {
		type: 'reversePrint',
		name: 'Pink',
		value: 'card-print-pink'
	},
	'227': {
		type: 'reversePrint',
		name: 'Metallic Gold',
		value: 'card-print-metallicgold'
	},

	// Text styles - fonts
	'300': {
		type: 'topTextFont',
		name: 'Helvetica',
		value: 'card-text-font-1'
	},
	'301': {
		type: 'topTextFont',
		name: 'Arial',
		value: 'card-text-font-2'
	},
	'302': {
		type: 'topTextFont',
		name: 'Times New Roman',
		value: 'card-text-font-3'
	},
	'303': {
		type: 'topTextFont',
		name: 'Georgia',
		value: 'card-text-font-4'
	},
	'304': {
		type: 'middleTextFont',
		name: 'Helvetica',
		value: 'card-text-font-1'
	},
	'305': {
		type: 'middleTextFont',
		name: 'Arial',
		value: 'card-text-font-2'
	},
	'306': {
		type: 'middleTextFont',
		name: 'Times New Roman',
		value: 'card-text-font-3'
	},
	'307': {
		type: 'middleTextFont',
		name: 'Georgia',
		value: 'card-text-font-4'
	},
	'308': {
		type: 'bottomTextFont',
		name: 'Helvetica',
		value: 'card-text-font-1'
	},
	'309': {
		type: 'bottomTextFont',
		name: 'Arial',
		value: 'card-text-font-2'
	},
	'30a': {
		type: 'bottomTextFont',
		name: 'Times New Roman',
		value: 'card-text-font-3'
	},
	'30b': {
		type: 'bottomTextFont',
		name: 'Georgia',
		value: 'card-text-font-4'
	},
	// --
	'30c': {
		type: 'reverseTopTextFont',
		name: 'Helvetica',
		value: 'card-text-font-1'
	},
	'30d': {
		type: 'reverseTopTextFont',
		name: 'Arial',
		value: 'card-text-font-2'
	},
	'30e': {
		type: 'reverseTopTextFont',
		name: 'Times New Roman',
		value: 'card-text-font-3'
	},
	'30f': {
		type: 'reverseTopTextFont',
		name: 'Georgia',
		value: 'card-text-font-4'
	},
	'310': {
		type: 'reverseMiddleTextFont',
		name: 'Helvetica',
		value: 'card-text-font-1'
	},
	'311': {
		type: 'reverseMiddleTextFont',
		name: 'Arial',
		value: 'card-text-font-2'
	},
	'312': {
		type: 'reverseMiddleTextFont',
		name: 'Times New Roman',
		value: 'card-text-font-3'
	},
	'313': {
		type: 'reverseMiddleTextFont',
		name: 'Georgia',
		value: 'card-text-font-4'
	},
	'314': {
		type: 'reverseBottomTextFont',
		name: 'Helvetica',
		value: 'card-text-font-1'
	},
	'315': {
		type: 'reverseBottomTextFont',
		name: 'Arial',
		value: 'card-text-font-2'
	},
	'316': {
		type: 'reverseBottomTextFont',
		name: 'Times New Roman',
		value: 'card-text-font-3'
	},
	'317': {
		type: 'reverseBottomTextFont',
		name: 'Georgia',
		value: 'card-text-font-4'
	},

	// Text styles - sizes
	'400': {
		type: 'topTextSize',
		name: 'Small',
		value: 'card-text-size-small'
	},
	'401': {
		type: 'topTextSize',
		name: 'Medium',
		value: 'card-text-size-medium'
	},
	'402': {
		type: 'topTextSize',
		name: 'Large',
		value: 'card-text-size-large'
	},
	'403': {
		type: 'middleTextSize',
		name: 'Small',
		value: 'card-text-size-small'
	},
	'404': {
		type: 'middleTextSize',
		name: 'Medium',
		value: 'card-text-size-medium'
	},
	'405': {
		type: 'middleTextSize',
		name: 'Large',
		value: 'card-text-size-large'
	},
	'406': {
		type: 'bottomTextSize',
		name: 'Small',
		value: 'card-text-size-small'
	},
	'407': {
		type: 'bottomTextSize',
		name: 'Medium',
		value: 'card-text-size-medium'
	},
	'408': {
		type: 'bottomTextSize',
		name: 'Large',
		value: 'card-text-size-large'
	},
	// --
	'409': {
		type: 'reverseTopTextSize',
		name: 'Small',
		value: 'card-text-size-small'
	},
	'40a': {
		type: 'reverseTopTextSize',
		name: 'Medium',
		value: 'card-text-size-medium'
	},
	'40b': {
		type: 'reverseTopTextSize',
		name: 'Large',
		value: 'card-text-size-large'
	},
	'40c': {
		type: 'reverseMiddleTextSize',
		name: 'Small',
		value: 'card-text-size-small'
	},
	'40d': {
		type: 'reverseMiddleTextSize',
		name: 'Medium',
		value: 'card-text-size-medium'
	},
	'40e': {
		type: 'reverseMiddleTextSize',
		name: 'Large',
		value: 'card-text-size-large'
	},
	'40f': {
		type: 'reverseBottomTextSize',
		name: 'Small',
		value: 'card-text-size-small'
	},
	'410': {
		type: 'reverseBottomTextSize',
		name: 'Medium',
		value: 'card-text-size-medium'
	},
	'411': {
		type: 'reverseBottomTextSize',
		name: 'Large',
		value: 'card-text-size-large'
	},

	// Text styles - decorations
	'500': {
		type: 'topTextBold',
		name: 'Normal',
		value: ''
	},
	'501': {
		type: 'topTextBold',
		name: 'Bold',
		value: 'card-text-bold'
	},
	'502': {
		type: 'topTextItalic',
		name: 'Normal',
		value: ''
	},
	'503': {
		type: 'topTextItalic',
		name: 'Italic',
		value: 'card-text-italic'
	},
	'504': {
		type: 'topTextUnderline',
		name: 'Normal',
		value: ''
	},
	'505': {
		type: 'topTextUnderline',
		name: 'Underline',
		value: 'card-text-underline'
	},
	'506': {
		type: 'middleTextBold',
		name: 'Normal',
		value: ''
	},
	'507': {
		type: 'middleTextBold',
		name: 'Bold',
		value: 'card-text-bold'
	},
	'508': {
		type: 'middleTextItalic',
		name: 'Normal',
		value: ''
	},
	'509': {
		type: 'middleTextItalic',
		name: 'Italic',
		value: 'card-text-italic'
	},
	'50a': {
		type: 'middleTextUnderline',
		name: 'Normal',
		value: ''
	},
	'50b': {
		type: 'middleTextUnderline',
		name: 'Underline',
		value: 'card-text-underline'
	},
	'50c': {
		type: 'bottomTextBold',
		name: 'Normal',
		value: ''
	},
	'50d': {
		type: 'bottomTextBold',
		name: 'Bold',
		value: 'card-text-bold'
	},
	'50e': {
		type: 'bottomTextItalic',
		name: 'Normal',
		value: ''
	},
	'50f': {
		type: 'bottomTextItalic',
		name: 'Italic',
		value: 'card-text-italic'
	},
	'510': {
		type: 'bottomTextUnderline',
		name: 'Normal',
		value: ''
	},
	'511': {
		type: 'bottomTextUnderline',
		name: 'Underline',
		value: 'card-text-underline'
	},
	// --
	'512': {
		type: 'reverseTopTextBold',
		name: 'Normal',
		value: ''
	},
	'513': {
		type: 'reverseTopTextBold',
		name: 'Bold',
		value: 'card-text-bold'
	},
	'514': {
		type: 'reverseTopTextItalic',
		name: 'Normal',
		value: ''
	},
	'515': {
		type: 'reverseTopTextItalic',
		name: 'Italic',
		value: 'card-text-italic'
	},
	'516': {
		type: 'reverseTopTextUnderline',
		name: 'Normal',
		value: ''
	},
	'517': {
		type: 'reverseTopTextUnderline',
		name: 'Underline',
		value: 'card-text-underline'
	},
	'518': {
		type: 'reverseMiddleTextBold',
		name: 'Normal',
		value: ''
	},
	'519': {
		type: 'reverseMiddleTextBold',
		name: 'Bold',
		value: 'card-text-bold'
	},
	'51a': {
		type: 'reverseMiddleTextItalic',
		name: 'Normal',
		value: ''
	},
	'51b': {
		type: 'reverseMiddleTextItalic',
		name: 'Italic',
		value: 'card-text-italic'
	},
	'51c': {
		type: 'reverseMiddleTextUnderline',
		name: 'Normal',
		value: ''
	},
	'51d': {
		type: 'reverseMiddleTextUnderline',
		name: 'Underline',
		value: 'card-text-underline'
	},
	'51e': {
		type: 'reverseBottomTextBold',
		name: 'Normal',
		value: ''
	},
	'51f': {
		type: 'reverseBottomTextBold',
		name: 'Bold',
		value: 'card-text-bold'
	},
	'520': {
		type: 'reverseBottomTextItalic',
		name: 'Normal',
		value: ''
	},
	'521': {
		type: 'reverseBottomTextItalic',
		name: 'Italic',
		value: 'card-text-italic'
	},
	'522': {
		type: 'reverseBottomTextUnderline',
		name: 'Normal',
		value: ''
	},
	'523': {
		type: 'reverseBottomTextUnderline',
		name: 'Underline',
		value: 'card-text-underline'
	},
};