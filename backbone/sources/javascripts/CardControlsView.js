var CardControlsView = Backbone.View.extend({
  render: function() {
    this.$el.html(this.template(this.model.attributes));
    return this;
  },
  events: {
  	'click .control': 'setOption'
  },
  setOption: function(e){
  	var option = e.target.attributes.rel.value;
  	this.model.set(cardOptions[option].type, option);
  }
});