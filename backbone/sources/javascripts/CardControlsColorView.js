var CardControlsColorView = CardControlsView.extend({
	template: _.template($('#tpl-controls-color').html()),
	initialize: function() {
		this.listenTo(this.model, 'change:color', this.render);
	}
});