var CardFlipView = Backbone.View.extend({
	className: 'card',
	template: _.template($('#tpl-card-flip').html()),
	events: {
		'click .flip.front': 'flipToFront',
		'click .flip.reverse': 'flipToReverse',
		'click .card-link-show': 'showLink',
    'click .card-link-hide': 'hideLink'
	},

	render: function(){
		this.$el.html(this.template(this.model.decodeOptions()));
    return this;
	},

	flipToFront: function(){
		if(this.$('.card-front').is(':hidden')){
			this.$('.card-front').show();
			this.$('.card-reverse').hide();
		}
	},

	flipToReverse: function(){
		if(this.$('.card-reverse').is(':hidden')){
			this.$('.card-reverse').show();
			this.$('.card-front').hide();
		}
	},

  showLink: function(){
    var input = this.$('.card-link-input').show().focus().get(0);
    input.setSelectionRange(0, input.value.length);
    this.$('.card-link-show').hide();
    this.$('.card-link-hide').show();
  },

  hideLink: function(){
    var input = this.$('.card-link-input').hide().get(0);
    input.setSelectionRange(0,0);
    this.$('.card-link-show').show();
    this.$('.card-link-hide').hide();
  }
});