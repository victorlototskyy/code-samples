var Card = Backbone.Model.extend({
	defaults: {
		color: '000',
		layout: '100',
		print: '209',
		topTextWording: 'The ABC company Ltd',
		topTextFont: '300',
		topTextSize: '402',
		topTextBold: '500',
		topTextItalic: '502',
		topTextUnderline: '504',
		middleTextWording: 'Better products, lower prices',
		middleTextFont: '304',
		middleTextSize: '404',
		middleTextBold: '506',
		middleTextItalic: '508',
		middleTextUnderline: '50a',
		hasBottomText: true,
		bottomTextWording: 'Head Office\nSomewhere road\nAnytown, AT17SW\nTel: 01567 987654\nEmail: info@abc.co.uk\nWebsite: www.abc.co.uk',
		bottomTextFont: '308',
		bottomTextSize: '407',
		bottomTextBold: '50c',
		bottomTextItalic: '50e',
		bottomTextUnderline: '510',
		startWithReverse: false,
		hasReverse: false,
		reverseLayout: '10a',
		reversePrint: '21d',
		reverseTopTextWording: 'Sample reverse top text',
		reverseTopTextFont: '30c',
		reverseTopTextSize: '40a',
		reverseTopTextBold: '512',
		reverseTopTextItalic: '514',
		reverseTopTextUnderline: '516',
		reverseMiddleTextWording: 'Sample reverse middle text',
		reverseMiddleTextFont: '310',
		reverseMiddleTextSize: '40d',
		reverseMiddleTextBold: '518',
		reverseMiddleTextItalic: '51a',
		reverseMiddleTextUnderline: '51c',
		reverseBottomTextWording: 'Sample reverse bottom text',
		reverseBottomTextFont: '314',
		reverseBottomTextSize: '410',
		reverseBottomTextBold: '51e',
		reverseBottomTextItalic: '520',
		reverseBottomTextUnderline: '522',
		logo: false
	},

	initialize: function(){
		this.listenTo(this, 'change:layout', this.layoutChanged);
		this.listenTo(this, 'change:reverseLayout', this.reverseLayoutChanged);
	},

	layoutChanged: function(model, value){
		var layoutsWithoutBottomText = ['107', '108', '109'];
		this.set('hasBottomText', _.indexOf(layoutsWithoutBottomText, value) == -1);
	},

	reverseLayoutChanged: function(model, value){
		var nonEditableLayouts = ['10a', '10c', '10d', '10e']; 
    this.set('hasReverse', _.indexOf(nonEditableLayouts, value) == -1);
	},

	getOptionName: function(option){
		return cardOptions[option].name;
	},

	serialize: function(){
		var data = {};
    var unconvertableOptions = ['logo','topTextWording','middleTextWording','bottomTextWording','reverseTopTextWording','reverseMiddleTextWording','reverseBottomTextWording'];
    _.each(this.attributes, function(code, option){
    	if(_.indexOf(unconvertableOptions, option) != -1){
        data[option] = code;
      }else{
      	if(!_.isUndefined(cardOptions[code])){
      		data[option] = cardOptions[code].name;
      	}
      }
    });
    return data;
	},

  decodeOptions: function(){
    var data = {};
    var textOptions = ['topTextWording','middleTextWording','bottomTextWording','reverseTopTextWording','reverseMiddleTextWording','reverseBottomTextWording'];
    _.each(this.attributes, function(code, option){
      if(!_.isUndefined(cardOptions[code])) {
        data[option] = cardOptions[code].value
      }else{
        if(_.indexOf(textOptions, option) != -1){
          data[option] = code.split('\n').join('<br>');
        }else{
          data[option] = code;
        }
      }
    });

    data['link'] = this.generateLink();

    return data;
  },

  generateLink: function(){
  	var link = 'http://premierplasticcards.co.uk/design_card_new.php#card/';
  	_.each(this.attributes, function(value){
  		link += encodeURIComponent(value) + '/'
  	});

  	return link;
  }
});