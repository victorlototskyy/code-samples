var CardControlsUploadView = CardControlsView.extend({
	template: _.template($('#tpl-controls-upload').html()),
	render: function() {
    this.$el.html(this.template(this.model.attributes));
    window.logo = new Dropzone("#logo-upload");
  	logo.on("addedfile", function(file) {
    	card.set('logo', file.name);
  	});
    return this;
  },
});