var CardControlsReverseWordingView = CardControlsView.extend({
	template: _.template($('#tpl-controls-reverse-wording').html()),
	events: {
		'keyup .control-text': 'changeText',
		'change .control-font': 'changeFont',
		'change .control-size': 'changeSize',
		'change .control-decoration' : 'changeDecoration'
	},

	changeText: function(e){
		this.model.set(e.target.name, _.escape(e.target.value));
	},

	changeFont: function(e){
		this.model.set(e.target.name, e.target.value);
	},

	changeSize: function(e){
		this.model.set(e.target.name, e.target.value);
	},

	changeDecoration: function(e){
		if(e.target.checked){
			this.model.set(e.target.name, e.target.value);
		}else{
			this.model.set(e.target.name, e.target.attributes.rel.value);
		}
	}
});